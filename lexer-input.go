// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package uparse

import (
	"fmt"
	"io"
	"strings"
)

//--
//-- LexerInput
//--

type LexerInput struct {
	parent         *LexerInput
	lexer          *Lexer
	buffer         []rune
	cursor         int
	selectionStack chan int
	next           bool
}

func NewLexerInput(lexer *Lexer, value string) *LexerInput {
	return &LexerInput{
		lexer:          lexer,
		buffer:         []rune(value),
		selectionStack: make(chan int, 128),
	}
}

func (in *LexerInput) Seek(position int, whence int) bool {
	if whence == io.SeekStart {
		in.cursor = position
	} else if whence == io.SeekCurrent {
		in.cursor += position
	} else if whence == io.SeekEnd {
		in.cursor += len(in.buffer)
	}
	if in.cursor < 0 {
		in.cursor = 0
		return false
	} else if in.cursor >= len(in.buffer) {
		in.cursor = len(in.buffer)
		return false
	}
	return true
}
func (in *LexerInput) Begin() *LexerInput {
	return &LexerInput{
		parent:         in,
		lexer:          in.lexer,
		buffer:         in.buffer,
		selectionStack: make(chan int, 128),
		cursor:         in.cursor,
	}
}

func (in *LexerInput) End() {
	in.parent.cursor = in.cursor
}

func (in *LexerInput) Peek() rune {
	return in.buffer[in.cursor]
}
func (in *LexerInput) Cursor() int {
	return in.cursor
}
func (in *LexerInput) Current() rune {
	return in.buffer[in.cursor]
}
func (in *LexerInput) CurrentString() string {
	return string(in.Current())
}
func (in *LexerInput) EOF() bool {
	return in.cursor >= len(in.buffer)
}
func (in *LexerInput) IsLastLine() bool {
	for _, v := range in.buffer[in.cursor:] {
		if v == '\n' {
			return false
		}
	}
	return true
}

func (in *LexerInput) Skip(count int) bool {
	if in.cursor+count >= len(in.buffer) {
		in.cursor = len(in.buffer)
		return false
	}
	in.cursor += count
	return true
}

func (in *LexerInput) SkipToStartOf(sequence string) bool {
	bufferStr := string(in.buffer[in.cursor:])
	if idx := strings.Index(bufferStr, sequence); idx > -1 {
		in.cursor = in.cursor + idx
		return true
	}
	return false
}
func (in *LexerInput) SkipToEndOf(sequence string) bool {
	if in.SkipToStartOf(sequence) {
		return in.Skip(len(sequence))
	}
	return false
}

func (in *LexerInput) StartsWith(value string) bool {
	return strings.HasPrefix(string(in.buffer[in.cursor:]), value)
}

func (in *LexerInput) BeginSelect() {
	in.selectionStack <- in.cursor
}
func (in *LexerInput) EndSelect() []rune {
	return in.buffer[<-in.selectionStack:in.cursor]
}

func (in *LexerInput) Next() bool {
	if len(in.buffer) == 0 {
		return false
	}
	if !in.next {
		in.next = true
		return true
	}

	if in.cursor+1 == len(in.buffer) {
		in.cursor++
		return false
	} else if in.cursor+1 > len(in.buffer) {
		return false
	}

	in.cursor++
	return true
}

//-- utility functions

// ExpectedChars returns up to “count“ chars
// if the buffer has more runes in it then requested, it appends ... to the return value
func (in *LexerInput) ExpectedChars(count int) (value string) {
	for i := in.cursor; i < len(in.buffer); i++ {
		if i == count+2 {
			value += "..."
			break
		}
		value += string(in.buffer[i])
	}
	return
}

func (in *LexerInput) Error(msg string) error {
	return fmt.Errorf("line %d at %d: %s", in.Line(), in.CharPosition(), msg)
}
func (in *LexerInput) ErrorExpected(expected string) error {
	return in.Error("expected " + expected + " got " + in.ExpectedChars(3))
}

func (in *LexerInput) Line() int {
	return strings.Count(string(in.buffer[:in.cursor]), "\n") + 1
}
func (in *LexerInput) CharPosition() (position int) {
	for i := in.cursor; i >= 0; i-- {
		if i < len(in.buffer) && in.buffer[i] == '\n' {
			break
		}
		position++
	}
	return
}
