// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package uparse

import (
	"errors"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

//--
//-- CommentDescription

type CommentDescription struct {
	StartSequence string
	StopSequence  string
	StopOperator  string
	LineComment   bool
}

//--
//-- OperatorDescription
//--

type OperatorDescription struct {
	Name          string
	StartSequence string
}

type StringDescription struct {
	StartSequence string
	StopSequence  string
	AllowNewline  bool
}

//--
//-- Lexer
//--

type Lexer struct {
	seperators            map[rune]bool
	operatorsDescriptions []*OperatorDescription
	stringDefinitions     []*StringDescription
	commentDefinitions    []*CommentDescription
	escapeChar            rune
}

func (lex *Lexer) SetEscapeChar(char rune) {
	lex.escapeChar = char
}
func (lex *Lexer) SetSeperators(seperators string) {
	for _, rne := range seperators {
		lex.seperators[rne] = true
	}
}
func (lex *Lexer) SetOperators(descriptions []*OperatorDescription) {
	lex.operatorsDescriptions = descriptions
	//-- sort longest start sequences first
	sort.Slice(lex.operatorsDescriptions, func(i, j int) bool {
		return len(lex.operatorsDescriptions[j].StartSequence) < len(lex.operatorsDescriptions[i].StartSequence)
	})
}
func (lex *Lexer) SetStringDefinitions(descriptions []*StringDescription) {
	lex.stringDefinitions = descriptions
}
func (lex *Lexer) SetCommentDefinitions(descriptions []*CommentDescription) {
	lex.commentDefinitions = descriptions
}
func (lex *Lexer) isSeperator(value rune) bool {
	return lex.seperators[value]
}
func (lex *Lexer) Lex(value string) (tokens TokenList, err error) {
	input := NewLexerInput(lex, value)

	// fmt.Printf("value: %v\n", value)
	// fmt.Printf("input.buffer: %v\n", input.buffer)

	escape := false
	var buffer []rune

	addToken := func(token Token) Token {
		tokens = append(tokens, token)
		return token
	}

	commitLiteral := func() {
		if len(buffer) > 0 {
			addToken(&LiteralToken{BaseToken: &BaseToken{lexer: lex, value: string(buffer), line: input.Line(), position: input.CharPosition() - 1}})
			buffer = nil
		}
	}

	handleEscapeSequences := func(in *LexerInput) (bool, error) {
		if !escape && in.Current() == lex.escapeChar {
			escape = true
		} else if escape {
			result := in.Current()
			switch in.Current() {
			case 'r':
				result = '\r'
			case 'n':
				result = '\n'
			case 't':
				result = '\t'
			case 'v':
				result = '\v'
			case 'b':
				result = '\b'
			case 'a':
				result = '\a'
			case 'x':
				if !in.Next() || !strings.Contains("0123456789abcdef", strings.ToLower(in.CurrentString())) {
					return false, in.ErrorExpected("hex digit")
				}
				if !in.Next() || !strings.Contains("0123456789abcdef", strings.ToLower(in.CurrentString())) {
					return false, in.ErrorExpected("hex digit")
				}

				in.Seek(-1, os.SEEK_CUR)
				in.BeginSelect()
				in.Skip(2)

				hexString := string(in.EndSelect())
				value, err := strconv.ParseInt(hexString, 16, 32)
				if err != nil {
					return false, errors.New("this should not happen???")
				}

				in.Seek(-1, os.SEEK_CUR)

				buffer = append(buffer, rune(value))

				escape = false
				return true, nil
			case 'u':
				if !in.Next() || !strings.Contains("0123456789abcdef", strings.ToLower(in.CurrentString())) {
					return false, in.ErrorExpected("hex digit")
				}
				if !in.Next() || !strings.Contains("0123456789abcdef", strings.ToLower(in.CurrentString())) {
					return false, in.ErrorExpected("hex digit")
				}
				if !in.Next() || !strings.Contains("0123456789abcdef", strings.ToLower(in.CurrentString())) {
					return false, in.ErrorExpected("hex digit")
				}
				if !in.Next() || !strings.Contains("0123456789abcdef", strings.ToLower(in.CurrentString())) {
					return false, in.ErrorExpected("hex digit")
				}

				in.Seek(-3, os.SEEK_CUR)
				in.BeginSelect()
				in.Skip(4)

				hexString := string(in.EndSelect())
				value, err := strconv.ParseInt(hexString, 16, 32)
				if err != nil {
					return false, errors.New("this should not happen???")
				}

				in.Seek(-1, os.SEEK_CUR)

				buffer = append(buffer, rune(value))

				escape = false
				return true, nil
			}
			buffer = append(buffer, result)
			escape = false
			return true, nil
		}
		return escape, nil
	}

	handleStrings := func() (bool, error) {
		lookahead := input.Begin()
		defer lookahead.End()

		for _, def := range lex.stringDefinitions {
			if lookahead.StartsWith(def.StartSequence) {
				line := lookahead.Line()
				position := lookahead.CharPosition()

				commitLiteral()

				lookahead.Skip(len(def.StartSequence))

				foundStopSequence := false
				for lookahead.Next() {
					if esc, err := handleEscapeSequences(lookahead); esc {
						continue
					} else if err != nil {
						return false, err
					}
					if lookahead.StartsWith(def.StopSequence) {
						lookahead.Skip(len(def.StopSequence) - 1)
						foundStopSequence = true
						break
					} else {
						buffer = append(buffer, lookahead.Current())
					}
				}

				if !foundStopSequence {
					return false, lookahead.ErrorExpected(def.StopSequence)
				}

				value := string(buffer)
				buffer = nil

				addToken(&StringToken{
					stringDefinition: *def,
					BaseToken:        &BaseToken{lexer: lex, line: line, position: position, value: value},
				})

				return true, nil
			}
		}

		return false, nil
	}

	//-
	//-- comments
	//-

	handleComments := func() (bool, error) {
		lookahead := input.Begin()
		defer lookahead.End()

		for _, def := range lex.commentDefinitions {
			if lookahead.StartsWith(def.StartSequence) {
				lookahead.Skip(len(def.StartSequence))
				lookahead.BeginSelect()

				if !lookahead.IsLastLine() && !lookahead.SkipToStartOf(def.StopSequence) {
					return false, lookahead.ErrorExpected(def.StopSequence)
				}

				if lookahead.IsLastLine() {
					lookahead.Seek(0, io.SeekEnd)
				} else if !lookahead.SkipToStartOf(def.StopSequence) {
					return false, lookahead.ErrorExpected(def.StopSequence)
				}

				value := string(lookahead.EndSelect())

				lookahead.Skip(len(def.StopSequence) - 1)

				commitLiteral()

				addToken(&CommentToken{
					commentDefinition: *def,
					BaseToken:         &BaseToken{lexer: lex, line: lookahead.Line(), position: lookahead.CharPosition(), value: value},
				})

				return true, nil
			}
		}

		return false, nil
	}

	handleOperators := func() (handled bool, err error) {
		lookahead := input.Begin()
		defer lookahead.End()

		line := lookahead.Line()
		position := lookahead.CharPosition()

		var operator *OperatorDescription
		for _, op := range lex.operatorsDescriptions {
			if lookahead.StartsWith(op.StartSequence) {
				lookahead.Skip(len(op.StartSequence) - 1)
				operator = op
				goto search_done
			}
		}

	search_done:

		if operator != nil {
			commitLiteral()
			addToken(&OperatorToken{
				operatorDescription: operator,
				BaseToken:           &BaseToken{lexer: lex, line: line, position: position, value: operator.Name},
			})
			handled = true
		}

		return
	}

	for input.Next() {
	seperators_finished:
		if input.EOF() {
			return
		}

		for lex.isSeperator(input.Current()) {
			commitLiteral()

			line := input.Line()
			position := input.CharPosition()
			addToken(&SeperatorToken{
				BaseToken: &BaseToken{lexer: lex, line: line, position: position, value: string(input.Current())},
			})

			if !input.Skip(1) {
				goto seperators_finished
			}
			continue
		}

		// if esc, err := handleEscapeSequences(input); esc {
		// 	continue
		// } else if err != nil {
		// 	return nil, err
		// }

		if ok, err := handleComments(); ok {
			continue
		} else if err != nil {
			return nil, err
		}

		if ok, err := handleOperators(); ok {
			continue
		} else if err != nil {
			return nil, err
		}

		if ok, err := handleStrings(); ok {
			continue
		} else if err != nil {
			return nil, err
		}

		buffer = append(buffer, input.Current())
	}

	commitLiteral()
	// addToken(&EOFToken{BaseToken: &BaseToken{lexer: lex, line: input.Line(), position: input.CharPosition(), eof: true}})

	return
}

func NewLexer() *Lexer {
	lexer := &Lexer{
		seperators:            make(map[rune]bool),
		operatorsDescriptions: make([]*OperatorDescription, 0),
		commentDefinitions:    make([]*CommentDescription, 0),
	}
	return lexer
}
