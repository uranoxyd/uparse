// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package uparse

import (
	"fmt"
	"strconv"
)

type Token interface {
	Line() int
	Position() int
	Value() string
	Type() string
	Description() any
}

type TokenList []Token

func (l TokenList) FilterTypes(types ...string) (out TokenList) {
	for _, token := range l {
		filtered := false
		for _, fType := range types {
			if token.Type() == fType {
				filtered = true
				break
			}
		}

		if !filtered {
			out = append(out, token)
		}
	}
	return
}

func (l TokenList) String() string {
	out := ""

	for _, token := range l {
		switch t := token.(type) {
		case *CommentToken:
			desc := t.Description().(*CommentDescription)
			out += desc.StartSequence + token.Value() + desc.StopSequence
		case *OperatorToken:
			desc := t.Description().(*OperatorDescription)
			out += desc.StartSequence
		case *SeperatorToken:
			out += token.Value()
		case *LiteralToken:
			out += token.Value()
		case *StringToken:
			desc := t.Description().(StringDescription)
			out += desc.StartSequence + token.Value() + desc.StopSequence
		}
	}

	return out
}
func (l TokenList) StrippedString(whitespace string) string {
	out := ""

	i := 0
	var lastToken Token
	for i < len(l) {
		numStripped := 0
		tokenBeforeStrip := lastToken
		for i < len(l) && l[i].Type() == "Seperator" {
			numStripped++
			i++
		}
		if i >= len(l) {
			break
		}
		if numStripped > 0 && out != "" && (l[i].Type() == "Literal" && tokenBeforeStrip != nil && tokenBeforeStrip.Type() == "Literal") {
			out += whitespace
		}

		token := l[i]
		switch t := token.(type) {
		case *CommentToken:
			// out += t.Definition().StartSequence + token.Value() + t.Definition().StopSequence
			// if t.Definition().LineComment {
			// 	out += "\n"
			// }
		case *OperatorToken:
			out += t.Description().(OperatorDescription).StartSequence
		// case *SeperatorToken:
		// 	out += token.Value()
		case *LiteralToken:
			out += token.Value()
		case *StringToken:
			out += t.Description().(StringDescription).StartSequence + token.Value() + t.Description().(StringDescription).StopSequence
		}

		lastToken = token
		i++
	}

	if out[len(out)-1] == ' ' {
		return out[:len(out)-1]
	}

	return out
}

//--
//-- BaseToken

type BaseToken struct {
	Token
	lexer    *Lexer
	line     int
	position int
	value    string
	// eof      bool
}

func (t *BaseToken) IsString() bool {
	return t.Type() == "String"
}
func (t *BaseToken) IsLiteral() bool {
	return t.Type() == "Literal"
}
func (t *BaseToken) IsOperator() bool {
	return t.Type() == "Operator"
}
func (t *BaseToken) IsSeperator() bool {
	return t.Type() == "Seperator"
}
func (t *BaseToken) IsOperatorOf(name string) bool {
	return t.Type() == "Operator" && t.value == name
}
func (t *BaseToken) Value() string {
	return t.value
}
func (t *BaseToken) Type() string {
	return "Base"
}
func (t *BaseToken) String() string {
	return fmt.Sprintf("%s at %d, %d: %s", "%s", t.line, t.position, strconv.Quote(t.value))
}
func (t *BaseToken) Line() int {
	return t.line
}
func (t *BaseToken) Position() int {
	return t.position
}
func (t *BaseToken) Description() any {
	return nil
}

//--
//-- SeperatorToken

type SeperatorToken struct {
	*BaseToken
}

func (t *SeperatorToken) Type() string {
	return "Seperator"
}
func (t *SeperatorToken) String() string {
	return fmt.Sprintf(t.BaseToken.String(), t.Type())
}
func (t *SeperatorToken) Description() any {
	return nil
}

//--
//-- LiteralToken

type LiteralToken struct {
	*BaseToken
}

func (t *LiteralToken) Type() string {
	return "Literal"
}
func (t *LiteralToken) IsLiteral() bool {
	return true
}
func (t *LiteralToken) String() string {
	return fmt.Sprintf(t.BaseToken.String(), t.Type())
}
func (t *LiteralToken) Description() any {
	return nil
}

//--
//-- StringToken

type StringToken struct {
	*BaseToken
	stringDefinition StringDescription
}

func (t *StringToken) Type() string {
	return "String"
}
func (t *StringToken) String() string {
	return fmt.Sprintf(t.BaseToken.String(), t.Type())
}
func (t *StringToken) Description() any {
	return t.stringDefinition
}

//--
//-- CommentToken

type CommentToken struct {
	*BaseToken
	commentDefinition CommentDescription
}

func (t *CommentToken) Type() string {
	return "Comment"
}
func (t *CommentToken) String() string {
	return fmt.Sprintf(t.BaseToken.String(), t.Type())
}
func (t *CommentToken) Description() any {
	return t.commentDefinition
}

//--
//-- OperatorToken

type OperatorToken struct {
	*BaseToken
	operatorDescription *OperatorDescription
}

func (t OperatorToken) IsOperatorOf(name string) bool {
	return t.Value() == name
}
func (t *OperatorToken) Type() string {
	return "Operator"
}
func (t *OperatorToken) String() string {
	return fmt.Sprintf(t.BaseToken.String(), t.Type())
}
func (t *OperatorToken) Description() any {
	return t.operatorDescription
}
